from fastapi import FastAPI
from pymongo import MongoClient

app = FastAPI()

client = MongoClient("mongodb://mongo:27017")
db = client["mydatabase"]

@app.get("/")
async def read_root():
    return {"Hello": "World"}